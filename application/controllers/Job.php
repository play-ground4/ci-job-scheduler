<?php
class Job extends CI_Controller
{
    public function index()
    {
        echo "Test Job" . PHP_EOL;
    }

    public function run($id)
    {
        if ($this->input->is_cli_request()) {
            echo "Running task where id = $id" . PHP_EOL;
        }
    }
}
